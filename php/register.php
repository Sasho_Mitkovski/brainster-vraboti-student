<?php

require_once "db.php";

$errFName = $errEmail = $errCompany = $errPhone = $errStudents = "";

//VALIDACIJA
$hasError = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['full_name']) || empty($_POST['full_name'])) {
        $errFName = "Внесето го вашето име и презиме";
        $hasError = true;
    }
    //email
    if (!isset($_POST['email']) || empty($_POST['email'])) {
        $errEmail = "Внесете ја вашата email адреса";
        $hasError = true;
    } else {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errEmail = "Неточен формат";
            $hasError = true;
        }
    }

    if (!isset($_POST['company']) || empty($_POST['company'])) {
        $errCompany = "Внесете има на вашата компанија";
        $hasError = true;
    }
    //phone
    if (!isset($_POST['phone']) || empty($_POST['phone'])) {
        $errPhone = "Внесете тел.број на компанијата";
        $hasError = true;
    }


    if (!isset($_POST['type_of_students']) || empty($_POST['type_of_students'])) {
        $errStudents = "Селектирајте тип на студент";
        $hasError = true;
    }
}

//PDO DATABAZA
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !$hasError) {

    $sql = "INSERT INTO registar 
        (type_of_students_id, full_name, company, email, phone) 
    VALUES 
        (:type_of_students_id, :full_name, :company, :email, :phone)";

    $data = [
        'type_of_students_id' => $_POST['type_of_students'],
        'full_name' => $_POST['full_name'],
        'company' => $_POST['company'],
        'email' => $_POST['email'],
        'phone' => $_POST['phone'],
    ];


    $query = $conn->prepare($sql);
    $e_uspeshno = $query->execute($data);

    if ($e_uspeshno) {
        header('Location: dashboard.php?success=Успешно испратено барање');
        die;
    } else {
        header('Location: forma.php?error=Try again');
        die;
    }
}
//Memory input
function is_post()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        return true;
    } else {
        return false;
    }
}

function input_sent($name)
{
    if (!isset($_POST[$name])) {
        return false;
    }

    if (empty($_POST[$name])) {
        return false;
    }

    return true;
}

function memory_input($ime_na_pole)
{
    if (!is_post()) {
        return '';
    }

    if (input_sent($ime_na_pole)) {
        return $_POST[$ime_na_pole];
    }

    return '';
}
//Delete
