<?php

require_once "db.php";

$sql = "DELETE FROM registar WHERE id = :id";
$query = $conn->prepare($sql);

if ($query->execute(['id' => $_POST['id']])) {
    header('Location: dashboard.php?success=Успешно избришана апликација');
    die;
} else {
    header('Location: dashboard.php?error=Неуспешно избришана апликација');
    die;
}
