<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/style.css">
  <title>Sasho MitkovskiWD1 proekt_01</title>
  <style>
    td {
      border: 1px solid rgb(10, 10, 10);
      padding: 6px;
    }

    th {
      border: 1px solid rgb(10, 10, 10);
      padding: 6px;
    }
  </style>

</head>

<body>
  <div class="container">
    <div class="row">

      <div class="col">
        <h2 class="text-center bg-secondary py-3 mt-3">Листа на студенти кои аплицирале за вработување</h2>
        <div class="row">
          <div class="col-3 offset-1 text-center">
            <?php if (isset($_GET['success']) && !empty($_GET['success'])) { ?>
              <div class=" alert-success my-3" role="alert">
                <?php echo $_GET['success'] ?>
              </div>
            <?php } ?>
          </div>
        </div>

        <table class="table table-striped w-100">

          <thead>
            <tr class="bg-success ">
              <th>ID</th>
              <th>Име и презиме</th>
              <th>Име на компанија</th>
              <th>Email адреса</th>
              <th>Телефонски број</th>
              <th>Тип на студент</th>
              <th>Избриши</th>

            </tr>
          </thead>
          <?php
          require_once "db.php";

          $query_students = $conn->query("SELECT * FROM type_of_students");

          $students = [];
          while ($row = $query_students->fetch()) {
            $students[$row['id']] = $row['type_of_students'];
          }

          $query = $conn->query("SELECT * FROM registar");

          if ($query->rowCount() == 0) {
            echo '<tr><td colspan="6">Не е најден ниту еден внес</td></tr>';
          } else {
            while ($row = $query->fetch()) {

              echo '<tr>';
              echo "<td>{$row['id']}</td>";
              echo "<td>{$row['full_name']}</td>";
              echo "<td>{$row['company']}</td>";
              echo "<td>{$row['email']}</td>";
              echo "<td>{$row['phone']}</td>";
              echo "<td>{$students[$row['type_of_students_id']]}</td>";
              echo "<td>
              <form action='delete.php' method='POST'>
                  <input type='hidden' name='id' value='{$row['id']}' />
                  <button type='submit' class='btn btn-danger'>Delete</button>
              </form>
          </td>";
              echo '</tr>';
            }
          }
          ?>

        </table>
        <a href="forma.php" class="btn btn-success my-3 px-4 ">Аплицирај</a>
      </div>
    </div>
  </div>
</body>

</html>