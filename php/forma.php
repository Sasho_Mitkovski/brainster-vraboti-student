<?php


require_once "db.php";
require "register.php";



$query = $conn->query("SELECT * FROM type_of_students");

$students = [];
while ($row = $query->fetch()) {
    $students[$row['id']] = $row['type_of_students'];
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/forma.css">
    <title>Sasho MitkovskiWD1 proekt_01</title>
    <title>Forma</title>
    <style>

    </style>
</head>

<body class="bg-warning">
    <nav class="navbar navbar-md-dark  navbar-expand-md bg-warning fixed-top row shadow">

        <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span><svg width="2.5em" height="2em" viewBox="0 0 16 16" class=" text-light" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4.5 11.5A.5.5 0 0 1 5 11h10a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 1 3h10a.5.5 0 0 1 0 1H1a.5.5 0 0 1-.5-.5z" />
                </svg></span>
        </button>

        <div class="collapse navbar-collapse text-white" id="mainNavbar">
            <ul class="navbar-nav w-100 ">
                <li class="nav-item  col-md-2 mobile-none my-auto">
                    <a class="  nav-link text-dark text-white text-center" href="../index.html">
                        <img src="../img/logo.png" class="logo"><br>
                        brainster
                    </a>

                </li>
                <li class="nav-item active col-md-2 my-auto">
                    <a class="nav-link text-dark text-white mt-1 " href="https://marketpreneurs.brainster.co/">Академија за маркетинг </a>
                </li>
                <li class="nav-item col-md-2 my-auto">
                    <a class="nav-link text-dark text-white mt-1" href="https://codepreneurs.brainster.co/">Акадеmija за
                        програмирање
                    </a>
                </li>

                <li class="nav-item col-md-2 my-auto">
                    <a class="nav-link text-dark text-white mt-1" href="#">Академија за data science<span class="sr-only"></span></a>
                </li>

                <li class="nav-item col-md-2 my-auto">
                    <a class="nav-link text-dark text-white mt-1 " href="https://design.brainster.co/">Академија за дизајн<span class="sr-only"></span></a>
                </li>

                <li class="nav-item  col-md-2 mt-2 my-auto margin100">
                    <a href="forma.php">
                        <button type="button" class="btn btn-red btn-desno">
                            Вработи наш студент
                        </button>
                    </a>
                </li>
            </ul>
        </div>

    </nav>
    <div class="container-fluid margin-70 h-100">

        <div class="row ">
            <div class="col ">
                <H1 class="text-center bold size-100 mt-4">Вработи студенти</H1>
            </div>
        </div>

        <div class="container forma">
            <form action="forma.php" method="POST">
                <div class="row bold">

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <input type="hidden" name="action-type" value="apliciraj" />
                                <label for="student_name">Име и Презиме</label>

                                <input class="form-control" type="text" name="full_name" id="student_name" placeholder="Вашето име и презиме" value="<?php echo memory_input('full_name') ?>">

                                <?php if ($errFName) { ?>
                                    <span class="required "><?php echo $errFName; ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="company">Име на компанија</label>
                            <input class="form-control" type="text" name="company" id="company" placeholder="Име на вашата компанија" value="<?php echo memory_input('company') ?>">

                            <?php if ($errCompany) { ?>
                                <span class="required "><?php echo $errCompany; ?></span>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <div class="row bold">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Контакт имеил</label>
                            <input class="form-control" type="text" name="email" id="email" placeholder="Контакт имејл на вашата компанија" value="<?php echo memory_input('email') ?>">

                            <?php if ($errEmail) { ?>
                                <span class="required "><?php echo $errEmail; ?></span>

                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Контакт телефон</label>
                            <input type="text" name="phone" class="form-control " id="phone" placeholder="Контакт телефон на вашата компанија" value="<?php echo memory_input('phone') ?>">

                            <?php if ($errPhone) { ?>
                                <span class="required "><?php echo $errPhone; ?></span>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row bold">
                    <div class="col-md-6">
                        <label for="type_of_students">Тип на студенти</label>
                        <div class="form-group">

                            <select class="custom-select select-height " id="type_of_students" name="type_of_students" class="mb-3">
                                <option selected="true" disabled="disabled">Тип на студенти</option>

                                <?php
                                foreach ($students as $key => $c) {
                                    echo "<option style='font-weight:bold;' value='{$key}'>{$c}</option>";
                                }
                                ?>

                            </select>
                            <?php if ($errStudents) { ?>
                                <span class="required "><?php echo $errStudents; ?></span>
                            <?php } ?>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group  bottom-margin">
                            <label for="submit">Испрати барање</label><br>
                            <button type="submit" class="btn btn-danger btn-block py-3 bold " id="submit">Испрати</button>

                        </div>
                    </div>
                </div>
            </form>

        </div>

        <div class="row bg-dark text-light bold fixed-bottom">
            <div class="col mt-3">
                <p class="text-center ">Изработка со
                    <span>
                        <i class="fa fa-heart" style="font-size:18px;color:red">
                        </i>
                    </span> од студентите на Brainster
                </p>
            </div>
        </div>
    </div>

</body>


</html>