-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2020 at 03:36 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vrabotuvanje`
--

-- --------------------------------------------------------

--
-- Table structure for table `registar`
--

CREATE TABLE `registar` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_of_students_id` int(10) UNSIGNED DEFAULT NULL,
  `full_name` varchar(32) DEFAULT NULL,
  `company` varchar(32) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `phone` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registar`
--

INSERT INTO `registar` (`id`, `type_of_students_id`, `full_name`, `company`, `email`, `phone`) VALUES
(1, 1, 'Sasho Mitkovski', 'Brainster', 'mitkovskis@gmail.com', '078256647');

-- --------------------------------------------------------

--
-- Table structure for table `type_of_students`
--

CREATE TABLE `type_of_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_of_students` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_of_students`
--

INSERT INTO `type_of_students` (`id`, `type_of_students`) VALUES
(1, 'Студенти од маркетинг'),
(2, 'Студенти од програмирање'),
(3, 'Студенти од data science'),
(4, 'Студенти од дизајн');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registar`
--
ALTER TABLE `registar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `registar_id_kon_country_id` (`type_of_students_id`);

--
-- Indexes for table `type_of_students`
--
ALTER TABLE `type_of_students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registar`
--
ALTER TABLE `registar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `type_of_students`
--
ALTER TABLE `type_of_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `registar`
--
ALTER TABLE `registar`
  ADD CONSTRAINT `registar_id_kon_country_id` FOREIGN KEY (`type_of_students_id`) REFERENCES `type_of_students` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
