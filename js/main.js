   //JavaScript
   document.querySelector("#coding").addEventListener("change", showProgramming);
   document.querySelector("#design").addEventListener("change", showDesign);
   document.querySelector("#marketing").addEventListener("change", showMarketing);

   function showProgramming() {
       if (document.querySelector("#coding").checked) {
           document.querySelector("#design").checked = false;
           document.querySelector("#marketing").checked = false;

           //NOVO: 
           //na klik na programing, od #filter-marketing - #filter-design trgni ja klasata .filter-active
           //a na elementot #filter-coding dodai ja klasata .filter-active
           //klasata filter-active dodadava samo background-color: crimson;
           document.querySelector("#filter-marketing").classList.remove('filter-active');
           document.querySelector("#filter-design").classList.remove('filter-active');
           document.querySelector("#filter-coding").classList.add('filter-active');

           hideAllCards();

           var codingCards = document.querySelectorAll(".coding");
           for (var i = 0; i < codingCards.length; i++) {
               codingCards[i].style.display = "inline-block";
           }
       } else {
           document.querySelector("#filter-coding").classList.remove('filter-active');
           showAllCards();
       }
   }


   function showDesign() {
       if (document.querySelector("#design").checked) {
           document.querySelector("#coding").checked = false;
           document.querySelector("#marketing").checked = false;

           document.querySelector("#filter-marketing").classList.remove('filter-active');
           document.querySelector("#filter-coding").classList.remove('filter-active');
           document.querySelector("#filter-design").classList.add('filter-active');

           hideAllCards();

           var designCards = document.querySelectorAll(".design");
           for (var i = 0; i < designCards.length; i++) {
               designCards[i].style.display = "inline-block";
           }
       } else {
           document.querySelector("#filter-design").classList.remove('filter-active');
           showAllCards();
       }
   }

   function showMarketing() {
       if (document.querySelector("#marketing").checked) {
           document.querySelector("#coding").checked = false;
           document.querySelector("#design").checked = false;

           document.querySelector("#filter-design").classList.remove('filter-active');
           document.querySelector("#filter-coding").classList.remove('filter-active');
           document.querySelector("#filter-marketing").classList.add('filter-active');

           hideAllCards();

           var marketingCards = document.querySelectorAll(".marketing");
           for (var i = 0; i < marketingCards.length; i++) {
               marketingCards[i].style.display = "inline-block";
           }
       } else {
           document.querySelector("#filter-marketing").classList.remove('filter-active');
           showAllCards();
       }
   }

   function hideAllCards() {
       var cards = document.querySelectorAll(".allcards");

       for (var i = 0; i < cards.length; i++) {
           cards[i].style.display = "none";
       }
   }


   function showAllCards() {
       var cards = document.querySelectorAll(".allcards");

       for (var i = 0; i < cards.length; i++) {
           cards[i].style.display = "inline-block";
       }
   }