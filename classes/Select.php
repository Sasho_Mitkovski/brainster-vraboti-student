<?php

class Select{

    private $name;
    private $options = [];

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
       return $this->name;
    }

    public function setOptions(array $options){
        $this->options = $options;
    }
    public function getOptions(){
       return $this->options;
    }
    public function theName(){
      echo  $this->name;
    }
    public function makeOptions(){
        foreach ($this->getOptions() as $option_text) {
            echo "<option value='$option_text'>$option_text</option>";
        }
    }
    
}

?>